#!/usr/bin/env bash

backup_name=$PVC-$(date +$DATE_FORMAT)

kubectl get volumesnapshot $backup_name &>/dev/null \
&& { echo Backup already exists ; exit 1; }

# Delete existing backups, if there are too many
xargs -r -n1 \
kubectl delete volumesnapshot \
< <( \
  kubectl get volumesnapshots \
  -l pvc=$PVC,frequency=$FREQUENCY \
  --sort-by=.metadata.creationTimestamp \
  -o jsonpath='{.items[*].metadata.name}' \
  | rev | cut -s -d\  -f $RETAIN- | rev \
)

# Create a new backup
echo "
apiVersion: snapshot.storage.k8s.io/v1beta1
kind: VolumeSnapshot
metadata:
  name: $backup_name
  labels:
    pvc: $PVC
    frequency: $FREQUENCY
spec:
  source:
    persistentVolumeClaimName: $PVC
" | kubectl create -f -
